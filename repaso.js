console.log(nombre);
let apellido = 'Cervantes';


//function saludo (saludo) {
//	return `${getNombre()} ${saludo}`;
//}

(function() {
	return 'jaime'
})()

// con return implicito debido a la defincion de funcion matematica
let saludo = saludo => `${getNombre()} ${saludo}`;


//function getNombre() {
//	return `${nombre} ${apellido}`;
//}

let getNombre = () => `${nombre} ${apellido}`;

// con return explecito, cuando le ponemos las llaves
// let getNombre = () => {
//	return `${nombre} ${apellido}`;
// }


var nombre = 'Jaime';

console.log(saludo('vete al baño, te hace daño'));


let persona = {
	nombre: "Jaime \"wey\"",
	apellido: "Cervantes",
	edad: 31,
	fuerza: null,
	sexo: undefined,
	vivo: true,
	fechaNacimiento: new Date("1987-10-28").toUTCString(),
	hijos: [
		{
			nombre: "Jaime hijo",
			apellido: "Cervantes",
			edad: 2,
			fuerza: "mucha"
		},
		{
			nombre: "Julio hijo",
			apellido: "Cervantes",
			edad: 15,
			fuerza: "mucha mucha"
		},
		{
			nombre: "Pedro hijo",
			apellido: "Cervantes",
			edad: 20,
			fuerza: "poca"
		}
	],
	direccion: {
		calle: "Melchor Ocampo",
		numero: 2,
		colonia: "Las Flores"
	},
	estadoCivil: "Complicado"
}


//persona.hijos.forEach(hijoActual => console.log(hijoActual))

function imprimirHijo(hijoActual) {
	console.log(hijoActual)
}

persona.hijos.forEach(imprimirHijo)

//console.log(persona.hijos[0], persona.hijos[0].nombre)
//console.log(persona.fechaNacimiento)

function saludarDia(texto) {
	return `Hola, buenos dias ${texto}`;
}


function saludarFinal(fnSaludar, nombre) {
	let saludoCompleto = fnSaludar(nombre);

	return saludoCompleto
}

console.log(saludarFinal(saludarDia, 'Jaime'))

function SerVivo (nombre, edad) {
	this.nombre = nombre;
	this.edad = edad
}

SerVivo.prototype.respirar = function () {
	return "Respirando...";
}

SerVivo.prototype.caminar = function () {
	return "Caminando...";
}

const person = new SerVivo('Jaime', 31);
console.log(person);

console.log(person.caminar())

class Servivo2 {
	constructor(props) {
		for(let key in props) {
			this[key] = props[key]
		}
	}

	respirar () {
		return "Respirando..."; 
	}

	caminar () {
		return "Caminando..."; 
	}
}
const sv2 = new Servivo2({ nombre: "Jaime", edad: 31 });
console.log(sv2);

class Persona extends Servivo2 {
	constructor(props) {
		super(props);
	}

	comer () {
		return "Comiendo...";
	}
}
const p2 = new Persona(persona);
console.log(p2);
console.log(p2.caminar(), p2.comer());

/*
const Persona = Object.create(new SerVivo("Jaime", 31))

Persona.prototype.getNombre = function () {
	return `${this.nombre} ${this.apellido}`;
}


alert(new Persona.caminar())*/
